package com.crni03.webdemo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.crni03.webdemo.model.Items;

public interface ItemsRepository extends CrudRepository<Items, Long>{

	List<Items> findById(Long id);

	List<Items> findByType(String type);

}
