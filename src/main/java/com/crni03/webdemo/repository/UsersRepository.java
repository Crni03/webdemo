package com.crni03.webdemo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.crni03.webdemo.model.Users;

public interface UsersRepository extends CrudRepository<Users, Long> {


	List<Users> findByMail(String mail);

	List<Users> findByFirstName(String firstName);

	List<Users> findByLastNameIgnoreCase(String lastName);

}
