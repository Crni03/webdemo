package com.crni03.webdemo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class WebDemoController {

	@Autowired
	ItemController itemController;
	@Autowired
	UserController userController;

	@RequestMapping(method = RequestMethod.POST, value = "/admin")
	public String admin(HttpServletRequest request, Model model) {
		String delete = request.getParameter("delete");
		String edit = request.getParameter("edit");
		String add = request.getParameter("add");
		String get = request.getParameter("get");
		if (delete != null) {
			System.out.println("delete not null: " + delete);
			if (delete.contentEquals("user")) {
			//	model.addAttribute("allItems", itemController.getAllItems());
				return userController.deleteUser(request, model);
			} else {
				model.addAttribute("allUsers", userController.getAllUsers());
				return itemController.deleteItem(request, model);
			}
		}
		if (edit != null) {
			System.out.println("edit not null: " + edit);
			if (edit.contentEquals("user"))
				return userController.editUser(request, model);
			else
				return itemController.editItem(request, model);
		}
		if (get != null) {
			System.out.println("get not null: " + get);
			model.addAttribute("allUsers", userController.getAllUsers());
			model.addAttribute("allItems", itemController.getAllItems());
			if (get.contentEquals("user")) {
				System.out.println("get not null: id:" + request.getParameter("id"));

				return userController.getUserById(request, model);
			} else
				return itemController.getItem(request, model);
		}
		if (add != null) {
			System.out.println("add not null: " + add);
			return itemController.addItem(request, model);
		}
		return "admin";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/admin")
	public String adminGet(HttpServletRequest request, Model model) {
		if (userController.getAdminLogged()) {
			model.addAttribute("allItems", itemController.getAllItems());
			model.addAttribute("allUsers", userController.getAllUsers());
			return "admin";
		} else
			return "redirect:/login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/login")
	public String adminLogin(HttpServletRequest request, Model model) {
		return userController.adminLogin(request, model);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/login")
	public String adminLoginGet(HttpServletRequest request, Model model) {
		return "login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/index")
	public String index(HttpServletRequest request, Model model) {
		String login = request.getParameter("login");
		String signup = request.getParameter("signup");
		if (login != null) {
			return userController.login(request, model);
		}
		if (signup != null) {
			return userController.addUser(request, model);
		}
		return "index";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/index")
	public String indexGet(HttpServletRequest request, Model model) {
		return "index";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/webshop")
	public String webshop(HttpServletRequest request, Model model) {
		String profile = request.getParameter("profile");
		System.out.println("/webshop: " + profile);

		if (profile != null) {
			return "redirect:/profile";
		} else {
			return "redirect:/webshop";
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/webshop")
	public void webshopGet(HttpServletRequest request, Model model) {
		model.addAttribute("userName", userController.getUserLoginName());
		model.addAttribute("allItems", itemController.getAllItems());
	}

	@RequestMapping(method = RequestMethod.POST, value = "/profile")
	public String profile(HttpServletRequest request, Model model) {
		return userController.editProfile(request, model);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/profile")
	public void profileGet(HttpServletRequest request, Model model) {
		model.addAttribute("profile", userController.getUser());
	}

}
