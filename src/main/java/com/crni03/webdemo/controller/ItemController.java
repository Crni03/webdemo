package com.crni03.webdemo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import com.crni03.webdemo.model.Items;
import com.crni03.webdemo.service.ItemsService;

@Controller
public class ItemController {


	@Autowired
	ItemsService iteser;
	
	Items item;
	
	
	public String getItem(HttpServletRequest request, Model model) {
		String id = request.getParameter("id");
		if (checkId(id, model)) {
			item = iteser.findById(Long.valueOf(id));
			model.addAttribute("getItem", item);
		}
		return "admin";
	}

	public List<Items> getAllItems() {
		return iteser.getAllItems();
	}

	public String addItem(HttpServletRequest request, Model model) {
		String type = request.getParameter("type");
		String name = request.getParameter("name");
		String price = request.getParameter("price");

		if (type != null && type.contentEquals("")) {
			model.addAttribute("msg", "unesite type");
			return "admin";
		}
		if (name != null && name.contentEquals("")) {
			model.addAttribute("msg", "unesite name");
			return "admin";
		}
		if (price != null && price.contentEquals("")) {
			model.addAttribute("msg", "unesite price");
			return "admin";
		}
		iteser.addItem(new Items(type, name, price));
		model.addAttribute("msg", "dodano");
		return "redirect:/admin";
	}

	public String deleteItem(HttpServletRequest request, Model model) {
		if (checkId(request.getParameter("id"), model)) {
			Long id = Long.valueOf(request.getParameter("id"));
			iteser.delete(id);
			model.addAttribute("allItems", getAllItems());
			return "redirect:/admin";
		}
		return "admin";
	}

	public String editItem(HttpServletRequest request, Model model) {
		if (checkId(request.getParameter("id"), model)) {
			Long id = Long.valueOf(request.getParameter("id"));
			String type = request.getParameter("type");
			String name = request.getParameter("name");
			String price = request.getParameter("price");
			iteser.editItem(id, new Items(type, name, price));
			model.addAttribute("msg", "uredeno");
			return "redirect:/admin";
		}
		return "admin";
	}

	private boolean checkId(String string, Model model) {
		if (string != null) {
			return true;
		} else {
			model.addAttribute("msg", "unsei id");
			return false;
		}
	}
}
