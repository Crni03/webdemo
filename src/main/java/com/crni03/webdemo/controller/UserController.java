package com.crni03.webdemo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import com.crni03.webdemo.model.Users;
import com.crni03.webdemo.service.UsersService;

@Controller
public class UserController {
	

	@Autowired
	UsersService useser;

	Users user;

	boolean adminLoged;
	
	

	public String addUser(HttpServletRequest request, Model model) {
		String firstName = request.getParameter("first_name");
		String lastName = request.getParameter("last_name");
		String mail = request.getParameter("mail");
		String password = request.getParameter("password");
		String repeatPassword = request.getParameter("password_repeat");
		if (firstName.contentEquals("")) {
			model.addAttribute("msg", "upisite ime");
			return "index";
		}
		if (lastName.contentEquals("")) {
			model.addAttribute("msg", "upisite prezime");
			return "index";
		}
		if (mail.contentEquals("") || !mail.contains("@")) {
			model.addAttribute("msg", "upisite mail");
			return "index";
		}
		if (password.contentEquals("") || repeatPassword.contentEquals("")) {
			model.addAttribute("msg", "upisite password");
			return "index";
		}
		if (useser.findByMail(mail) != null) {
			model.addAttribute("msg", "taj mail se vec koristi");
			return "index";
		}
		if (password.contentEquals(repeatPassword)) {
			user = new Users(firstName, lastName, mail, password);
			useser.addUser(user);
			return "redirect:/webshop";
		} else {
			System.out.println("krivi pass");
			model.addAttribute("msg", "lozinke se ne podudaraju");
			return "index";
		}
	}

	public String login(HttpServletRequest request, Model model) {
		// TODO Auto-generated method stub
		System.out.println("login");
		String password = request.getParameter("password");
		String mail = request.getParameter("mail");
		if (mail.contentEquals("") || !mail.contains("@")) {
			model.addAttribute("msg", "upisite mail");
			return "index";
		}
		if (password.contentEquals("")) {
			model.addAttribute("msg", "upisite lozinku");
			return "index";
		}
		user = useser.findByMail(mail);
		if (password.contentEquals(user.getPassword())) {
			System.out.println("webshop userlogin: " + user.getPassword());
			return "redirect:/webshop";
		} else {
			System.out.println("wrong password");
			model.addAttribute("msg", "kriva lozinka");
			return "index";
		}
	}
	
	public String getUserLoginName() {
		// TODO Auto-generated method stub
		return "Dobrodosao\n" + user.getFirstName() + " " + user.getLastName();
	}

	
	public String adminLogin(HttpServletRequest request, Model model) {
		// TODO Auto-generated method stub

		String password = request.getParameter("password");
		String mail = request.getParameter("mail");
		if (mail.contentEquals("") || !mail.contains("@")) {
			model.addAttribute("msg", "upisite mail");
			return "login";
		}
		if (password.contentEquals("")) {
			model.addAttribute("msg", "upisite lozinku");
			return "login";
		}
		user = useser.findByMail(mail);
		if (password.contentEquals(user.getPassword())) {
			if (user.getStatus().contentEquals("admin")) {
				System.out.println("webshop userlogin: " + user.getPassword());
				adminLoged = true;
				return "redirect:/admin";
			} else {
				System.out.println("webshop userlogin: " + user.getStatus());
				System.out.println("webshop userlogin: " + user.getPassword());
				System.out.println("webshop userlogin: " + user.getMail());

				model.addAttribute("msg", "niste admin");
				return "index";

			}
		} else {
			System.out.println("wrong password");
			model.addAttribute("msg", "kriva lozinka");
			return "login";
		}
	}

	public List<Users> getAllUsers() {
		// TODO Auto-generated method stub
		return useser.getAllUsers();
	}

	public String getUserById(HttpServletRequest request, Model model) {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		if (checkId(id, model)) {
			user = useser.findById(Long.valueOf(id));
			model.addAttribute("getUser", user);
		}
		return "admin";
	}

	public String deleteUser(HttpServletRequest request, Model model) {
		System.out.println("deleteUser:");
		if (checkId(request.getParameter("id"), model)) {
			Long id = Long.valueOf(request.getParameter("id"));
			useser.delete(id);
			model.addAttribute("allUsers", getAllUsers());
		}
		return "admin";
	}

	public String editUser(HttpServletRequest request, Model model) {
		if (checkId(request.getParameter("id"), model)) {
			Long id = Long.valueOf(request.getParameter("id"));
			String status = request.getParameter("status");
			useser.editUser(id, status);
			model.addAttribute("msg", "obrisano");
			return "redirect:/admin";
		}
		return "admin";
	}

	private boolean checkId(String string, Model model) {
		if (string != null) {
			return true;
		} else {
			model.addAttribute("msg", "unsei id");
			return false;
		}
	}

	public String editProfile(HttpServletRequest request, Model model) {
		// TODO Auto-generated method stub
		String firstName = request.getParameter("first_name");
		String lastName = request.getParameter("last_name");
		String mail = request.getParameter("mail");
		String password = request.getParameter("password");
		if (firstName.contentEquals(""))
			firstName = user.getFirstName();
		if (lastName.contentEquals(""))
			lastName = user.getLastName();
		if (mail.contentEquals(""))
			mail = user.getMail();
		if (password.contentEquals(""))
			password = user.getPassword();

		System.out.println("/profile: firstName: " + firstName + " lastName: " + lastName + " mail: " + mail
				+ " password: " + password);
		useser.editProfile(user.getId(), user.getStatus(), new Users(firstName, lastName, mail, password));
		return "redirect:/webshop";
	}

	public Users getUser() {		return this.user;
	}

	public boolean getAdminLogged() {
		return this.adminLoged;
	}
}
