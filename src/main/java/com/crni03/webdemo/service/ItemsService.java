package com.crni03.webdemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crni03.webdemo.model.Items;
import com.crni03.webdemo.model.Users;
import com.crni03.webdemo.repository.ItemsRepository;

@Service
public class ItemsService {

	@Autowired
	ItemsRepository iterepo;

	public String populate() {
		iterepo.save(new Items("hrana", "banana", "15"));
		iterepo.save(new Items("pice", "cedevita", "10"));
		iterepo.save(new Items("odjeca", "majica", "52"));
		iterepo.save(new Items("hrana", "but", "14"));
		iterepo.save(new Items("odjeca", "cipele", "142"));
		iterepo.save(new Items("pice", "fanta", "12"));
		return "index";
	}

	public void addItem(Items item) {
		iterepo.save(item);
	}

	public Items findById(Long id) {
		Items item = iterepo.findOne(id);		
		return item;
		
	}
	public void findByType(String type) {
		iterepo.findByType(type);
	}

	public void delete(Long id) {
		iterepo.delete(id);
	}

	public void editItem(Long id, Items item) {
		item.setId(id);
		iterepo.save(item);

	}

	public List<Items> getAllItems() {
		// TODO Auto-generated method stub		
		List<Items> items = (List<Items>) iterepo.findAll();		
		return items;
	}

}
