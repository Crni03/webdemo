package com.crni03.webdemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crni03.webdemo.model.Users;
import com.crni03.webdemo.repository.UsersRepository;

@Service
public class UsersService {

	@Autowired
	UsersRepository userepo;

	public void addUser(Users user) {
		userepo.save(user);
	}

	public Users findById(Long id) {
		Users user = userepo.findOne(id);	
		return user;		
	}

	public void delete(Long id) {
		userepo.delete(id);
	}

	public void editProfile(Long id, String status, Users user) {
		user.setId(id);
		user.setStatus(status);
		userepo.save(user);

	}

	public void editUser(Long id, String status) {
		Users user = userepo.findOne(id);
		user.setStatus(status);
		user.setId(id);
		userepo.save(user);
	}

	public Users findByMail(String mail) {
		// TODO Auto-generated method stub
		Users user = null;
		for (Users us : userepo.findByMail(mail)) {
			user = us;
		}
		return user;
	}

	public List<Users> getAllUsers() {
		// TODO Auto-generated method stub
		List<Users> users = (List<Users>) userepo.findAll();
		return users;
	}

	public boolean exists(Long id) {
		// TODO Auto-generated method stub
		return userepo.exists(id);
	}

}
