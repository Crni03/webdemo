package com.crni03.webdemo.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.ui.Model;

public class WebDemoControllerTest {



	WebDemoController wdc = mock(WebDemoController.class);

	MockHttpServletRequest request = new MockHttpServletRequest();
	Model model = mock(Model.class);

	
	@Test
	public void adminTest() {
		when(wdc.admin(request, model)).thenCallRealMethod();
		assertEquals("admin", wdc.admin(request, model));

	}

	@Test
	public void adminLoginGetTest() {
		when(wdc.adminLoginGet(request, model)).thenCallRealMethod();
		assertEquals("login", wdc.adminLoginGet(request, model));

	}

	@Test
	public void indexTest() {
		when(wdc.index(request, model)).thenCallRealMethod();
		assertEquals("index", wdc.index(request, model));

	}
	@Test
	public void webshopTestProfile() {
		request.addParameter("profile", "");
		when(wdc.webshop(request, model)).thenCallRealMethod();
		assertEquals("redirect:/profile", wdc.webshop(request, model));

	}	@Test
	public void webshopTest() {
		when(wdc.webshop(request, model)).thenCallRealMethod();
		assertEquals("redirect:/webshop", wdc.webshop(request, model));

	}
}
